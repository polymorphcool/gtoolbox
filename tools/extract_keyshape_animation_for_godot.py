import bpy

action_keys = [ 
('Key','Key|Take 001|BaseLayer'), 
('Key.001','Key.001|Take 001|BaseLayer')]

print( "##########################################################" )


dict = {}
    
for k in action_keys:
    
    shapekey = bpy.data.shape_keys[ k[0] ]
 
    for block in shapekey.key_blocks:
        print( 'key', block.name )
        dict[ block.name ] = {
            'times':[],
            'values':[]
        }

    action = bpy.data.actions[ k[1] ]
    for fc in action.fcurves:
        print( fc.data_path )
        name = fc.data_path.replace( 'key_blocks["', '' ).replace( '"].value', '' )
        for kf in fc.keyframe_points:
            dict[name]['times'].append( kf.co[0] / 30.0 )
            dict[name]['values'].append( kf.co[1] )

ks = []
for d in dict:
    ks.append(d)
ks.sort()

for k in ks:
    print( k )
    out = '"times": PoolRealArray( '
    for i in range(0, len(dict[k]['times'])):
        if i > 0:
            out += ','
        out += str( dict[k]['times'][i] )
    out += ' ),\n"transitions": PoolRealArray('
    for i in range(0, len(dict[k]['times'])-1):
        if i > 0:
            out += ','
        out += '1'
    out += '),\n"update": 0,\n"values": ['
    for i in range(0, len(dict[k]['values'])):
        if i > 0:
            out += ','
        out += str( dict[k]['values'][i] )
    out += ']\n'
    print( out )
    #print( "\t", dict[d]['times'] )
    #print( "\t", dict[d]['values'] )