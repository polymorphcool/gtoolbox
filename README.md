# Godot toolbox

Assets and compilation script for polymorph projects. 

## Install all polymorph addons

An installation script (install.sh) can be run to clone and recompile the latest version of godot with all polymorph addons.

List of addons that the script will install:

- [gosc](https://gitlab.com/frankiezafe/gosc)
- [softskin](https://gitlab.com/frankiezafe/softskin)
- [futari](https://gitlab.com/polymorphcool/futari-addon)
- [gsyphon](https://gitlab.com/frankiezafe/gsyphon)