tool

extends MeshInstance

export(float,-180,180) var rotx = 0
export(float,-180,180) var roty = 0
export(float,-180,180) var rotz = 0

export(float,-180,180) var rotr = 0
export(float,-180,180) var rotg = 0
export(float,-180,180) var rotb = 0
export(float,0,1) var red_min = 0
export(float,0,1) var red_max = 1
export(float,0,1) var green_min = 0
export(float,0,1) var green_max = 1
export(float,0,1) var blue_min = 0
export(float,0,1) var blue_max = 1

export(float,0.01,100) var scale_min = 1
export(float,0.01,100) var scale_max = 1
export(float,0.01,100) var scale_freq = 1

var scale_a = 0
var color = Vector3(0,0,0)

func _ready():
	pass

func _process(delta):
	rotation_degrees += Vector3(rotx,roty,rotz) * delta
	scale_a += scale_freq * delta
	var s = ( 1.0 + sin(scale_a) ) * 0.5
	s = scale_min + s * (scale_max-scale_min)
	scale = Vector3(s,s,s)
	color.x += rotr * delta
	color.y += rotg * delta
	color.z += rotb * delta
	var r = red_min + ( 1.0 + sin(color.x) ) * 0.5 * (red_max-red_min)
	var g = green_min + ( 1.0 + sin(color.y) ) * 0.5 * (green_max-green_min)
	var b = blue_min + ( 1.0 + sin(color.z) ) * 0.5 * (blue_max-blue_min)
	material_override.albedo_color = Color(r,g,b)