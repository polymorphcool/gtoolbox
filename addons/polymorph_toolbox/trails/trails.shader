shader_type canvas_item;
render_mode blend_mix,unshaded;

void fragment() {
	
	// blur bg
	vec3 screen_tex = 
		texture( SCREEN_TEXTURE, UV ).rgb * 0.76 + 
		texture( SCREEN_TEXTURE, UV + vec2(1.0/800.0, 0.0) ).rgb * 0.06 + 
		texture( SCREEN_TEXTURE, UV - vec2(1.0/800.0, 0.0) ).rgb * 0.06 + 
		texture( SCREEN_TEXTURE, UV + vec2(0.0, 1.0/600.0) ).rgb * 0.06 + 
		texture( SCREEN_TEXTURE, UV - vec2(0.0, 1.0/600.0) ).rgb * 0.06;
	
	vec4 color_tex = texture( TEXTURE, UV );
	COLOR = mix( vec4( screen_tex.rgb * 0.99, 1.0 ), vec4( color_tex.rgb, 1.0 ), color_tex.a );
}