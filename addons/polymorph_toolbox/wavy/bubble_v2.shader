//http://developer.amd.com/wordpress/media/2012/10/ShaderX_Bubble.pdf

shader_type spatial;
render_mode blend_mix,depth_draw_alpha_prepass,cull_back,diffuse_burley,specular_schlick_ggx;

uniform float M_PI = 3.14159265359;

// bubble params
uniform vec3 freq;
uniform vec3 range;
uniform sampler2D noise : hint_white;
uniform vec3 noise_freq;
uniform vec3 noise_strength;
uniform vec2 noise_uv_speed;
uniform sampler2D panorama : hint_white;

// standard params
uniform vec4 albedo : hint_color;
uniform sampler2D texture_albedo : hint_albedo;
uniform float specular;
uniform float metallic;
uniform float roughness : hint_range(0,1);
uniform float point_size : hint_range(0,128);
uniform sampler2D texture_metallic : hint_white;
uniform vec4 metallic_texture_channel;
uniform sampler2D texture_roughness : hint_white;
uniform vec4 roughness_texture_channel;
uniform vec3 uv1_scale;
uniform vec3 uv1_offset;
uniform vec3 uv2_scale;
uniform vec3 uv2_offset;
uniform vec2 uv_noise_scale;

void vertex() {
	
	vec3 cnormal;
	vec3 ctangent;
	vec3 cbinormal;
	cnormal = normalize( VERTEX );
	if ( abs( cnormal.y ) == 1.0 ) {
		ctangent = normalize( cross( cnormal, vec3(0.0,0.0,1.0) ) );
	} else {
		ctangent = normalize( cross( cnormal, vec3(0.0,1.0,0.0) ) );
	}
	cbinormal = normalize( cross( cnormal, ctangent ) );

	// UV
	UV = UV * uv1_scale.xy + uv1_offset.xy;
	vec4 p = INV_PROJECTION_MATRIX * vec4( VERTEX * TIME * 1.89, 1.0 );
	float offset = float( INSTANCE_ID );
	//vec2( cnormal.x + cnormal.z, cnormal.y + cnormal.z )
	vec3 noise_rgb = texture( noise, vec2( cnormal.x + cnormal.z, cnormal.y + cnormal.z ) * uv_noise_scale + ( noise_uv_speed * TIME ) ).rgb;
	vec3 noise_s = noise_rgb * noise_strength;
	vec3 noise_f = noise_rgb * noise_freq;
	// motion along normal
	float t = 1000.0 + TIME;
	vec3 vx = cnormal * ( range.x+noise_s.x ) * sin( t * freq.x + t * noise_f.x );
	vec3 vy = ctangent * ( range.y+noise_s.y ) * sin( t * freq.y + t * noise_f.y );
	vec3 vz = cbinormal * ( range.z+noise_s.z ) * sin( t * freq.z + t * noise_f.z );
	VERTEX = VERTEX + vx + vy + vz;

}

vec4 texturePanorama(vec3 normal, sampler2D pano) {
	vec2 st = vec2(
			atan(normal.x, normal.z),
			acos(normal.y));
	if (st.x < 0.0) {
		st.x += M_PI * 2.0;
	}
	st /= vec2(M_PI * 2.0, M_PI);
	return textureLod(pano, st, 0.0);
}

void fragment() {
	vec2 base_uv = UV;
	vec4 albedo_tex = texture(texture_albedo,base_uv);
	ALBEDO = albedo.rgb * albedo_tex.rgb;
	float metallic_tex = dot(texture(texture_metallic,base_uv),metallic_texture_channel);
	METALLIC = metallic_tex * metallic;
	float roughness_tex = dot(texture(texture_roughness,base_uv),roughness_texture_channel);
	ROUGHNESS = roughness_tex * roughness;
	SPECULAR = specular;
	ALPHA = albedo.a;
//	ALBEDO = NORMAL;
}