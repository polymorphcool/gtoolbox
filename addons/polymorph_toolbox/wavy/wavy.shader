//http://developer.amd.com/wordpress/media/2012/10/ShaderX_Bubble.pdf

shader_type spatial;
render_mode blend_mix,depth_draw_opaque,cull_back,diffuse_burley,specular_schlick_ggx;

uniform float M_PI = 3.14159265359;

// bubble params
uniform vec3 freq;
uniform vec3 range;
uniform sampler2D noise : hint_white;
uniform vec3 noise_strength;
uniform vec2 noise_uv_speed;
uniform sampler2D panorama : hint_white;

// standard params
uniform vec4 albedo : hint_color;
uniform sampler2D texture_albedo : hint_albedo;
uniform float specular;
uniform float metallic;
uniform float roughness : hint_range(0,1);
uniform float point_size : hint_range(0,128);
uniform sampler2D texture_metallic : hint_white;
uniform vec4 metallic_texture_channel;
uniform sampler2D texture_roughness : hint_white;
uniform vec4 roughness_texture_channel;
uniform vec3 uv1_scale;
uniform vec3 uv1_offset;
uniform vec3 uv2_scale;
uniform vec3 uv2_offset;
uniform sampler2D texture_normal : hint_white;
uniform float normal_mult;

void vertex() {
	
	UV=UV*uv1_scale.xy+uv1_offset.xy;
	mat4 mat_world = mat4(
		normalize(CAMERA_MATRIX[0])*length(WORLD_MATRIX[0]),
		normalize(CAMERA_MATRIX[1])*length(WORLD_MATRIX[0]),
		normalize(CAMERA_MATRIX[2])*length(WORLD_MATRIX[2]),
		WORLD_MATRIX[3]
	);
	mat_world = mat_world * mat4( 
		vec4(cos(INSTANCE_CUSTOM.x),-sin(INSTANCE_CUSTOM.x),0.0,0.0), 
		vec4(sin(INSTANCE_CUSTOM.x),cos(INSTANCE_CUSTOM.x),0.0,0.0),
		vec4(0.0,0.0,1.0,0.0),
		vec4(0.0,0.0,0.0,1.0)
	);
	MODELVIEW_MATRIX = INV_CAMERA_MATRIX * mat_world;
	
	NORMAL = normalize( VERTEX );
	if ( abs( NORMAL.y ) == 1.0 ) {
		TANGENT = normalize( cross( NORMAL, vec3(0.0,0.0,1.0) ) );
	} else {
		TANGENT = normalize( cross( NORMAL, vec3(0.0,1.0,0.0) ) );
	}
	BINORMAL = normalize( cross( NORMAL, TANGENT ) );

	// UV
	UV = UV * uv1_scale.xy + uv1_offset.xy;
	vec4 p = MODELVIEW_MATRIX * vec4( VERTEX * TIME * 0.0089, 1.0 );
	float offset = mat_world[0].x;
	vec3 noise_rgb = texture( noise, UV + ( noise_uv_speed * p.x * TIME ) ).rgb * noise_strength;
	// motion along normal
	vec3 vx = NORMAL * range.x * sin( offset + TIME * ( freq.x + noise_rgb.x ) );
	vec3 vy = TANGENT * range.y * sin( offset + TIME * ( freq.y + noise_rgb.y ) );
	vec3 vz = BINORMAL * range.z * sin( offset + TIME * ( freq.z + noise_rgb.z ) );
	VERTEX = VERTEX + vx + vy + vz;

}

vec4 texturePanorama(vec3 normal, sampler2D pano) {
	vec2 st = vec2(
			atan(normal.x, normal.z),
			acos(normal.y));
	if (st.x < 0.0) {
		st.x += M_PI * 2.0;
	}
	st /= vec2(M_PI * 2.0, M_PI);
	return textureLod(pano, st, 0.0);
}

void fragment() {
	vec2 base_uv = UV;
	vec4 albedo_tex = texture(texture_albedo,base_uv);
	albedo_tex *= COLOR;
	ALBEDO = albedo.rgb * albedo_tex.rgb;
	float metallic_tex = dot(texture(texture_metallic,base_uv),metallic_texture_channel);
	METALLIC = metallic_tex * metallic;
	float roughness_tex = dot(texture(texture_roughness,base_uv),roughness_texture_channel);
	ROUGHNESS = roughness_tex * roughness;
	SPECULAR = specular;
	ALPHA = albedo.a * albedo_tex.a;
	NORMALMAP = texture(texture_normal,base_uv).rgb;
//	METALLIC = min( 0.0, NORMALMAP.y ) * 10.0;
	NORMALMAP_DEPTH = normal_mult;
}