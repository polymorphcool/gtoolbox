shader_type canvas_item;
render_mode blend_mix;

uniform vec3 mixer; // x: 0,1 or 2, y: 0,1 or 2, z: mix value [0,1]
uniform vec4 hcg_current;
//uniform vec2 limit_x_from;
//uniform vec2 limit_y_from;
//uniform vec2 limit_x_to;
//uniform vec2 limit_y_to;

uniform float grid;

uniform float PI = 3.1415926535897;
uniform vec4 checkboard_even = vec4(0.45,0.45,0.45,1.45);
uniform vec4 checkboard_odd = vec4(0.55,0.55,0.55,1.55);

// h: [0 ,360]
// c: [0,1]
// g: [0,1]

float xyz_min( in float x, in float y, in float z ) {
	if ( x <= y && x <= z ) {
		return x;
	} else if ( y >= z ) {
		return y;
	}
	return z;
}

float xyz_max( in float x, in float y, in float z ) {
	if ( x >= y && x >= z ) {
		return x;
	} else if ( y >= z ) {
		return y;
	}
	return z;
}

vec4 hcg_convert(in float r1, in float g1, in float b1, in float m, in float a ) {
	return vec4((r1 + m), (g1 + m), (b1 + m), a);
}

vec4 hcg2rgb( in float h, in float c, in float g, in float a ) {
	float x = c * (1.0 - abs(mod((h / 60.0), 2.0) - 1.0));
	float m = g * (1.0 - c);
	if ( h < 60.0 ) { return hcg_convert(c, x, 0.0, m, a); }
	else if ( h < 120.0 ) { return hcg_convert(x, c, 0.0, m, a); }
	else if ( h < 180.0 ) { return hcg_convert(0.0, c, x, m, a); }
	else if ( h < 240.0 ) { return hcg_convert(0.0, x, c, m, a); }
	else if ( h < 300.0 ) { return hcg_convert(x, 0.0, c, m, a); }
	else { return hcg_convert(c, 0.0, x, m, a); }
}

vec4 rgb2hcg( in float r, in float g, in float b, in float a ) {
	float min = xyz_min(r,g,b);
	float max = xyz_max(r,g,b);
	float d = max - min;
	float h = 0.0;
	// UNTESTED!
	// original code was: float gr = min && min / (1.0 - d);
	float gr = min / (1.0 - d);
	if (r == max) { 
		float m = 0.0;
		if ( g < b ) { m = 1.0; }
		h = (g - b) / d + m * 6.0;
	} else if (g == max) {
		h = (b - r) / d + 2.0;
	} else { 
		h = (r - g) / d + 4.0;
	}
	h *= 60.0;
	return vec4( h,d,gr,a );
}

vec4 hcg_default( in float x, in float y ) {
	int ix = int( x * grid );
	int iy = int( y * grid );
	ix += (iy%2 == 1)? 1 : 0;
	return mix( (ix%2 == 0)? checkboard_even : checkboard_odd, hcg2rgb( hcg_current.x * 360.0, hcg_current.y, hcg_current.z, 1.0 ), hcg_current.w );
}

vec4 hcg_x( in float a, in float r ) {
	return hcg2rgb( a * 360.0, r, hcg_current.z, 1.0 );
}

vec4 hcg_y( in float x, in float y ) {
	if ( x >= 0.0 ) {
		return hcg2rgb( hcg_current.x * 360.0, x, 1.0 - y, 1.0 );
	} else {
		return hcg2rgb( hcg_current.x * 360.0 + 180.0, abs(x), 1.0 - y, 1.0 );
	}
}

vec4 hcg_z( in float x, in float y, in float a, in float r ) {
	int ix = int( x * grid );
	int iy = int( y * grid );
	ix += (iy%2 == 1)? 1 : 0;
	return mix( (ix%2 == 0)? checkboard_even : checkboard_odd, hcg2rgb( a, hcg_current.y, hcg_current.z, 1.0 ), r );
}

void fragment() {
	
	vec2 base = vec2( 1.0-UV.x, 1.0-UV.y );
	vec2 cUV = base.xy - vec2(0.5,0.5);
	vec2 cUV2 = cUV * 2.0;
	float radius = min( 1, cUV2.x * cUV2.x + cUV2.y * cUV2.y );
	float angle = ( PI + atan( cUV2.y, cUV2.x ) ) / ( 2.0 * PI );
	
	vec4 c0 = vec4(0,0,0,1);
	vec4 c1 = vec4(0,0,0,1);
	
	if ( mixer.x == 0.0 ) {
		c0 = hcg_x( angle, radius );
	} else if ( mixer.x == 1.0 ) {
		c0 = hcg_y( UV.x, UV.y );
	} else if ( mixer.x == 2.0 ) {
		c0 = hcg_z( base.x, base.y, angle * 360.0, radius );
	} else {
		c0 = hcg_default( base.x, base.y );
	}
	
	if ( mixer.y == 0.0 ) {
		c1 = hcg_x( angle, radius );
	} else if ( mixer.y == 1.0 ) {
		c1 = hcg_y( UV.x, UV.y );
	} else if ( mixer.y == 2.0 ) {
		c1 = hcg_z( base.x, base.y, angle * 360.0, radius );
	} else {
		c1 = hcg_default( base.x, base.y );
	}
	
	COLOR = mix( c0, c1, mixer.z );
	
//	if ( mixer.x == 0.0 && mixer.y == 1.0 ) {
//		vec4 c0 = hcg_x( angle * 360.0, radius );
//		vec4 c1 = hcg_y( cUV2.x, UV.y );
//		COLOR = mix( c0, c1, mixer.z );
//	} else if ( mixer.x == 1.0 && mixer.y == 2.0 ) {
//		vec4 c0 = hcg_y( cUV2.x, UV.y );
//		vec4 c1 = hcg_z( UV.x, UV.y, angle * 360.0, radius );
//		COLOR = mix( c0, c1, mixer.z );
//	} else if ( mixer.x == 0.0 && mixer.y == 2.0 ) {
//		vec4 c0 = hcg_x( angle * 360.0, radius );
//		vec4 c1 = hcg_z( UV.x, UV.y, angle * 360.0, radius );
//		COLOR = mix( c0, c1, mixer.z );
//	} else {
//		COLOR = hcg2rgb( angle * 360.0, radius, 1.0, 1.0 );
//	}
	
	// HC
//	COLOR = hcg2rgb( angle * 360.0, radius, 1.0, 1.0 );
	
	// HG - not used
//	COLOR = hcg2rgb( angle * 360.0, 0.5, radius, 1.0 );

	// CG
//	if ( cUV2.x >= 0.0 ) {
//		COLOR = hcg2rgb( 120.0, cUV2.x, 1.0 - UV.y, 1.0 );
//	} else {
//		COLOR = hcg2rgb( 120.0 + 360.0, abs(cUV2.x), 1.0 - UV.y, 1.0 );
//	}

	// HA
//	int ix = int( UV.x * 10.0 );
//	int iy = int( UV.y * 10.0 );
//	ix += (iy%2 == 1)? 1 : 0;
//	COLOR = mix( (ix%2 == 0)? checkboard_even : checkboard_odd, hcg2rgb( angle * 360.0, 1.0, 1.0, 1.0 ), radius );
	
}