extends Node2D

var picker = null
var picker_size = Vector2(200,200)
var picker_active = false
var picker_icon_current = null
var picker_icon_target = null
var preview = null
var preview_plain = null
var preview_alpha = null
var preview_checkboard = null

var color_hcga = Color( 0,0,0,1 )
var color_target = color_hcga
						#   HUE         CHROMA         	GREY         ALPHA
var color_constrains = [ Vector2(0,1), Vector2(0,1), Vector2(0,1), Vector2(0,1) ]
var color_locks = [ false, false, false, false ]
var color_mixer = Vector3(-1,-1,0);

var constrain0 = Vector2(0,1)
var constrain1 = Vector2(0,1)
var u_constrain = constrain0
var v_constrain = constrain1

var color_transition = 0.15
var color_transition_current = 0

var mouse_cross = []
var rel_mouse = Vector2()
var last_valid_mouse = Vector2()
var mouse_cross_transition = 0.25
var mouse_cross_transition_current = 0

var _INIT = false

enum PLANE{ NONE = -1, HC = 0, CG = 1, HA = 2 }
var current_plane = PLANE.NONE

signal color_updated

# SETTERS

func v3_min(x,y,z):
	if x <= y and x <=z:
		return x
	elif y <= z:
		return y
	return z

func v3_max(x,y,z):
	if x >= y and x >=z:
		return x
	elif y >= z:
		return y
	return z

func set_rgba( c ):
	var cmin = v3_min(c.r,c.g,c.b)
	var cmax = v3_max(c.r,c.g,c.b)
	var d = cmax - cmin
	var h = 0.0;
	# original code was: float gr = min && min / (1.0 - d);
	var gr = 0
	if d < 1.0:
		gr = cmin / (1.0 - d)
	if c.r == cmax:
		var m = 0.0;
		if c.g < c.b:
			m = 1.0
		h = (c.g - c.b)
		if d > 0.0:
			h /= d
		h += m * 6.0
	elif c.g == cmax:
		h = (c.b - c.r) / d + 2.0
	else:
		h = (c.r - c.g) / d + 4.0
	h /= 6;
	
	color_target = Color( h,d,gr,c.a )
	picker_icon_target.position = get_picker_position( color_target )

#func set_constrain( axis, v2 ):
#
#	var ax = int(axis)
#	if not v2 is Vector2 or ax < 0 or ax > 3:
#		return
#
#	color_constrains[ax] = v2
#	match ax:
#		0:
#			color_hcga.r = apply_constrain( color_hcga.r, color_constrains[0], true )
#		1:
#			color_hcga.g = apply_constrain( color_hcga.g, color_constrains[1] )
#		2:
#			color_hcga.b = apply_constrain( color_hcga.b, color_constrains[2] )
#		3:
#			color_hcga.a = apply_constrain( color_hcga.a, color_constrains[3] )
#
#	color_target = color_hcga
#
#	match current_plane:
#		PLANE.HC:
#			picker_icon_target.position = Vector2( color_target.r, color_target.g ) * picker_size
#		PLANE.CV:
#			picker_icon_target.position = Vector2( color_target.g, color_target.b ) * picker_size
#		PLANE.VH:
#			picker_icon_target.position = Vector2( color_target.b, color_target.r ) * picker_size
#
#	change_plane( current_plane )
#	broadcast()
#	color_transition_current = color_transition

func set_lock( axis, enabled ):

	var ax = int(axis)
	if not enabled is bool or ax < 0 or ax > 3:
		return

	color_locks[ax] = enabled

	change_plane( current_plane )
	color_transition_current = color_transition

func resize( v2 ):

	if not v2 is Vector2:
		return
	
	if v2.x < 130:
		v2.x = 130
	if v2.y < 20:
		v2.y = 20
	
	picker_size = v2

	if _INIT:
		$vpc.rect_size = picker_size
		$vpc/vp/picker.rect_size = picker_size
		relocate_picker_current(true)
		$hc.rect_position.y = picker_size.y + 4
		$cg.rect_position.y = picker_size.y + 4
		$ha.rect_position.y = picker_size.y + 4
		$vp_preview.size.x = picker_size.x - 90
		$vp_preview/plain.scale.x = picker_size.x - 110
		$vp_preview/checkboard.position.x = picker_size.x - 110
		$vp_preview/alpha.position.x = picker_size.x - 110
		preview.position.y = picker_size.y

# GETTERS

func hcg_convert(var r1, var g1, var b1, var m, var a ):
	return Color((r1 + m), (g1 + m), (b1 + m), a);

func get_rgba():
	return hcga_to_rgba( color_hcga.r, color_hcga.g, color_hcga.b, color_hcga.a )

func get_rgb():
	var c = get_rgba()
	return Color( c.r, c.g, c.b, 1 )

# utils

func hcga_to_rgba( var hue, var chroma, var grey, var alpha ):
	var h = hue * 360
	var x = chroma * (1.0 - abs(fmod((h / 60.0), 2.0) - 1.0))
	var m = grey * (1.0 - chroma)
	if h < 60.0:
		return hcg_convert(chroma, x, 0.0, m, alpha)
	elif h < 120.0:
		return hcg_convert(x, chroma, 0.0, m, alpha)
	elif h < 180.0:
		return hcg_convert(0.0, chroma, x, m, alpha)
	elif h < 240.0:
		return hcg_convert(0.0, x, chroma, m, alpha)
	elif h < 300.0:
		return hcg_convert(x, 0.0, chroma, m, alpha)
	else:
		return hcg_convert(chroma, 0.0, x, m, alpha)
	

# PROCESSING

func apply_constrain( f, constrain, cyclic = false ):

	if cyclic:
		while f < 0:
			f += 1
		while f > 1:
			f -= 1
	else:
		if f < 0:
			f = 0
		if f > 1:
			f = 1

	if constrain != null and (constrain.x != 0 or constrain.y != 1):
		if constrain.x < constrain.y:
			if f < constrain.x or f > constrain.y:
				if f < constrain.x and cyclic:
					f += 1
				elif f < constrain.x:
					f = constrain.x
				var d = f - constrain.y
				if d > ( 1 - ( constrain.y - constrain.x ) ) * 0.5:
					return constrain.x
				else:
					return constrain.y
		elif constrain.x > constrain.y:
			if f < constrain.x and f > constrain.y:
				var d = f - constrain.y
				if d > ( constrain.x - constrain.y ) * 0.5:
					return constrain.x
				else:
					return constrain.y

	return f

func update_preview( force_transparent = false ):
	var pa = preview.material.get_shader_param( "alpha" )
	if force_transparent:
		pa = 0
	elif color_transition_current >= 0:
		var pc = color_transition_current / color_transition
		if color_mixer.y == PLANE.NONE:
			if pa > pc:
				pa = pc
			picker_icon_current.color.a = pc
			picker_icon_target.color.a = pc
		elif color_mixer.x == PLANE.NONE:
			if pa < 1 - pc:
				pa = 1 - pc
			picker_icon_current.color.a = 1 - pc
			picker_icon_target.color.a = 1 - pc
		
	preview_plain.color = get_rgb()
	preview_alpha.color = get_rgba()
	preview_checkboard.material.set_shader_param( "alpha", 1 )
	preview.material.set_shader_param( "alpha", pa )
	
func update_picker( force = false ):

	if!_INIT:
		return
	if color_transition_current >= 0:
		update_preview()
		color_mixer.z = ( 1 + sin( PI * 0.5 + ( color_transition_current / color_transition ) * PI ) ) * 0.5
		picker.material.set_shader_param( "mixer", color_mixer )

func update_target( r,g,b,a ):
	if r != -1:
		if !color_locks[0]:
			color_target.r = apply_constrain( r,  color_constrains[0], true )
	if g != -1:
		if !color_locks[1]:
			color_target.g = apply_constrain( g,  color_constrains[1] )
	if b != -1:
		if !color_locks[2]:
			color_target.b = apply_constrain( b,  color_constrains[2] )
	if a != -1:
		if !color_locks[3]:
			color_target.a = apply_constrain( a,  color_constrains[3] )

func move_cross( cross, pos, axis ):
	cross.position = pos
	if axis == 'X':
		if cross.position.y - cross.scale.y * 0.5 < 0:
			cross.position.y = cross.scale.y * 0.5
		if cross.position.y + cross.scale.y * 0.5 > picker_size.y:
			cross.position.y = picker_size.y - cross.scale.y * 0.5
	elif axis == 'Y':
		if cross.position.x - cross.scale.x * 0.5 < 0:
			cross.position.x = cross.scale.x * 0.5
		if cross.position.x + cross.scale.x * 0.5 > picker_size.x:
			cross.position.x = picker_size.x - cross.scale.x * 0.5

func update_picker_cross( delta ):

	if!_INIT:
		return

	if inside_picker() or picker_active:

		while rel_mouse.x < 0:
			rel_mouse.x = 0
		while rel_mouse.x > picker_size.x:
			rel_mouse.x = picker_size.x
		while rel_mouse.y < 0:
			rel_mouse.y = 0
		while rel_mouse.y > picker_size.y:
			rel_mouse.y = picker_size.y

		if mouse_cross_transition_current < mouse_cross_transition:
			mouse_cross_transition_current += delta
			if mouse_cross_transition_current > mouse_cross_transition:
				mouse_cross_transition_current = mouse_cross_transition
			var pc = mouse_cross_transition_current / mouse_cross_transition
			mouse_cross[0].scale.y = pc * picker_size.y
			mouse_cross[1].scale.x = pc * picker_size.x

		move_cross( mouse_cross[0], rel_mouse, 'X' )
		move_cross( mouse_cross[1], rel_mouse, 'Y' )

		last_valid_mouse = rel_mouse

		if picker_active:
			var cmouse = ( ( rel_mouse / picker_size ) - Vector2(0.5,0.5) ) * 2
			match current_plane:
				PLANE.HC:
					var a = atan2( cmouse.y, cmouse.x )
					while a < 0:
						a += TAU
					var rad = min( 1, cmouse.length() )
					update_target( a / (2*PI), rad, -1, -1 )
					picker_icon_target.position = get_picker_position( color_target )
				PLANE.CG:
					var relm =rel_mouse / picker_size
					update_target( -1, relm.x, 1 - relm.y, -1 )
					picker_icon_target.position = get_picker_position( color_target )
				PLANE.HA:
					var a = atan2( cmouse.y, cmouse.x )
					while a < 0:
						a += TAU
					var rad = min( 1, cmouse.length() )
					update_target( a / (2*PI), -1, -1, rad )
					picker_icon_target.position = get_picker_position( color_target )

	elif mouse_cross_transition_current > 0:

		mouse_cross_transition_current -= delta
		if mouse_cross_transition_current < 0:
			mouse_cross_transition_current = 0
		var pc = mouse_cross_transition_current / mouse_cross_transition
		mouse_cross[0].scale.y = pc * picker_size.y
		mouse_cross[1].scale.x = pc * picker_size.x

		move_cross( mouse_cross[0], last_valid_mouse, 'X' )
		move_cross( mouse_cross[1], last_valid_mouse, 'Y' )

func get_picker_position( hcga ):
	
	if !_INIT:
		return

	match current_plane:
		
		PLANE.HC:
			var a = hcga.r * 2 * PI
			var cosa = cos(a)
			var sina = sin(a)
			var radius = hcga.g * picker_size.x * 0.5
			return Vector2( picker_size.x * 0.5 + cosa * radius, picker_size.y * 0.5 + sina * radius )
			
		PLANE.CG:
			return Vector2( hcga.g * picker_size.x, ( 1 - hcga.b ) * picker_size.y )
			
		PLANE.HA:
			var a = hcga.r * 2 * PI
			var cosa = cos(a)
			var sina = sin(a)
			var radius = hcga.a * picker_size.x * 0.5
			return Vector2( picker_size.x * 0.5 + cosa * radius, picker_size.y * 0.5 + sina * radius )

func relocate_picker_current( apply_on_target ):

	if !_INIT:
		return
	
	picker_icon_current.position = get_picker_position( color_hcga )

	if apply_on_target:
		picker_icon_target.position = picker_icon_current.position

func change_plane( p ):

	if !_INIT:
		return

	match current_plane:
		PLANE.NONE:
			pass
		PLANE.HC:
#			preview.material.set_shader_param( "limit_x_from", color_constrains[0] )
#			preview.material.set_shader_param( "limit_y_from", color_constrains[1] )
			pass
		PLANE.CG:
#			preview.material.set_shader_param( "limit_x_from", color_constrains[1] )
#			preview.material.set_shader_param( "limit_y_from", color_constrains[2] )
			pass
		PLANE.HA:
#			preview.material.set_shader_param( "limit_x_from", color_constrains[0] )
#			preview.material.set_shader_param( "limit_y_from", color_constrains[3] )
			pass

	if p != current_plane:
		current_plane = p
		relocate_picker_current( true )

	match p:
		PLANE.NONE:
			color_mixer = Vector3( color_mixer.y,-1,0 )
		PLANE.HC:
			color_mixer = Vector3( color_mixer.y,0,0 )
#			preview.material.set_shader_param( "limit_x_to", color_constrains[0] )
#			preview.material.set_shader_param( "limit_y_to", color_constrains[1] )
		PLANE.CG:
			color_mixer = Vector3( color_mixer.y,1,0 )
#			preview.material.set_shader_param( "limit_x_to", color_constrains[1] )
#			preview.material.set_shader_param( "limit_y_to", color_constrains[2] )
		PLANE.HA:
			color_mixer = Vector3( color_mixer.y,2,0 )
#			preview.material.set_shader_param( "limit_x_to", color_constrains[0] )
#			preview.material.set_shader_param( "limit_y_to", color_constrains[3] )

func inside_picker():
	rel_mouse = get_viewport().get_mouse_position()
	rel_mouse -= get_global_transform().get_origin()
	if rel_mouse.x >= 0 and rel_mouse.x <= picker_size.x and rel_mouse.y >= 0 and rel_mouse.y <= picker_size.y:
		return true
	return false

func inside_widget():
	rel_mouse = get_viewport().get_mouse_position()
	rel_mouse -= get_global_transform().get_origin()
	if rel_mouse.x >= 0 and rel_mouse.x <= picker_size.x and rel_mouse.y >= 0 and rel_mouse.y <= picker_size.y + 20:
		return true
	return false

func shortest_path( current, target, constrain ):
	var right = 0.0
	var left = 0.0
	if current == target:
		return 0
	if current < target:
		right = target - current
		left = 1 - right
	else:
		left = current - target
		right = 1 - left
	# is the interpolation cross the inaccessible area?
	if constrain != null and (constrain.x != 0 or constrain.y != 1):
		if constrain.x < constrain.y:
			if current < target:
				return right
			else:
				return -left
		elif constrain.x > constrain.y:
			if current < target and current < constrain.y and target > constrain.y:
				return -left
			elif current > target and current > constrain.x and target < constrain.x:
				return right
	if left > right:
		return right
	return -left

func constrain_path( current, target, constrain ):
	var right = 0.0
	var left = 0.0
	if current == target:
		return 0
	if current < target:
		right = target - current
		left = 1 - right
	else:
		left = current - target
		right = 1 - left
	# is the interpolation cross the inaccessible area?
	if constrain != null and (constrain.x != 0 or constrain.y != 1):
		if constrain.x < constrain.y:
			if current < target:
				return right
			else:
				return -left
		elif constrain.x > constrain.y:
			if current < target and current < constrain.y and target > constrain.y:
				return -left
			elif current > target and current > constrain.x and target < constrain.x:
				return right
	return target - current

func color_interpolation( delta ):

	if color_hcga != color_target:
		var rm = shortest_path( color_hcga.r, color_target.r, color_constrains[0] )
		var gm = constrain_path( color_hcga.g, color_target.g, color_constrains[1] )
		var bm = constrain_path( color_hcga.b, color_target.b, color_constrains[2] )
		var am = constrain_path( color_hcga.a, color_target.a, color_constrains[3] )
		var mm = min( 1, delta * 5 )
		color_hcga.r += rm * mm 
		color_hcga.g += gm * mm
		color_hcga.b += bm * mm
		color_hcga.a += am * mm
		clamp_color()
		
		picker.material.set_shader_param( "hcg_current", color_hcga )
		update_preview()
		
		var ig
		var c
		
		ig = 1 - color_hcga.b
		while( ig < 0 ):
			ig += 1
		c = hcga_to_rgba(0,0,ig*ig,picker_icon_current.color.a)
		picker_icon_current.color = c
		
		ig = 1 - color_target.b
		while( ig < 0 ):
			ig += 1
		c = hcga_to_rgba(0,0,ig*ig,picker_icon_target.color.a)
		picker_icon_target.color = c
		if picker_active:
			c = hcga_to_rgba(0,0,ig*ig,1)
			mouse_cross[0].color = c
			mouse_cross[1].color = c
		
		broadcast()
	
	if !picker_active:
		mouse_cross[0].color = Color(1,1,1,1)
		mouse_cross[1].color = Color(1,1,1,1)

	relocate_picker_current( false )

func _ready():

	picker = $vpc/vp/picker
	picker_icon_current = $vpc/vp/current
	picker_icon_target = $vpc/vp/target
	if $vpc/vp/mousex != null:
		mouse_cross.append( $vpc/vp/mousex )
	if $vpc/vp/mousey != null:
		mouse_cross.append( $vpc/vp/mousey )
	preview = $preview
	preview_plain = $vp_preview/plain
	preview_alpha = $vp_preview/alpha
	preview_checkboard = $vp_preview/checkboard
	
	_INIT = picker != null
	_INIT = _INIT and preview != null
	_INIT = _INIT and preview_plain != null
	_INIT = _INIT and preview_alpha != null
	_INIT = _INIT and preview_checkboard != null
	_INIT = _INIT and picker_icon_current != null 
	_INIT = _INIT and picker_icon_target != null 
	_INIT = _INIT and mouse_cross.size() == 2

	if !_INIT:
		print( "Impossible to run this script smoothly! Check children nodes!" )
		return
	
	change_plane( PLANE.HC )
	color_transition_current = color_transition

	mouse_cross[0].scale.y = 0
	mouse_cross[1].scale.x = 0
	
	update_preview(true)
	picker.material.set_shader_param( "hcg_current", color_hcga )

	set_process_input(true)

func _input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed and inside_picker():
			picker_active = true
		else:
			picker_active = false
	elif event is InputEventMouseMotion:
		inside_picker()

func _process(delta):

	if!_INIT:
		return

	# updating picker
	if color_transition_current > 0:
		color_transition_current -= delta
		if color_transition_current < 0:
			color_transition_current = 0
		update_picker()
	# updating mouse cross
	update_picker_cross( delta )
	# updating rotation
	color_interpolation(delta)
	
	if !picker_active and !inside_widget() and color_mixer.y != -1:
		color_mixer = Vector3( color_mixer.y,-1,0 )
		color_transition_current = color_transition
	if inside_widget() and color_mixer.y == -1:
		change_plane( current_plane )
		color_transition_current = color_transition

func clamp_color():
	# HUE is cyclic!
	while color_hcga.r < 0:
		color_hcga.r += 1
	while color_hcga.r > 1:
		color_hcga.r -= 1
	# all other dimensions are linear
	if color_hcga.g < 0:
		color_hcga.g = 0
	if color_hcga.g > 1:
		color_hcga.g = 1
	if color_hcga.b < 0:
		color_hcga.b = 0
	if color_hcga.b > 1:
		color_hcga.b = 1
	if color_hcga.a < 0:
		color_hcga.a = 0
	if color_hcga.a > 1:
		color_hcga.a = 1

# SIGNALS

func broadcast():
	emit_signal( "color_updated" )

# SUB PARTS

func _on_hc_pressed():
	change_plane( PLANE.HC )
	color_transition_current = color_transition

func _on_cg_pressed():
	change_plane( PLANE.CG )
	color_transition_current = color_transition

func _on_ha_pressed():
	change_plane( PLANE.HA )
	color_transition_current = color_transition
