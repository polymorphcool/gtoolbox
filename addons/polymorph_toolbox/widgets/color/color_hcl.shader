shader_type canvas_item;
render_mode blend_mix;

uniform float PI = 3.1415926535897;
uniform float HCLgamma = 3.0;
uniform float HCLy0 = 100.0;
uniform float HCLmaxL = 0.530454533953517; // == exp(HCLgamma / HCLy0) - 0.5

float frac( float v ) {
	return v - floor(v);
}

// https://www.harding.edu/gclayton/color/topics/001_huevaluechroma.html
// http://www.chilliant.com/rgb2hsv.html
// https://en.wikipedia.org/wiki/Munsell_color_system
// HCG https://github.com/acterhd/hcg-color
vec3 HCLtoRGB(in vec3 HCL) {
	vec3 RGB = vec3( 0.0, 0.0, 0.0 );
	if (HCL.z != 0.0) {
		float H = HCL.x;
		float C = HCL.y;
		float L = HCL.z * HCLmaxL;
		float Q = exp((1.0 - C / (2.0 * L)) * (HCLgamma / HCLy0));
		float U = (2.0 * L - C) / (2.0 * Q - 1.0);
		float V = C / Q;
		float A = (H + min(frac(2.0 * H) / 4.0, frac(-2.0 * H) / 8.0)) * PI * 2.0;
		float T;
		H *= 6.0;
		if (H <= 0.999) {
			T = tan(A);
			RGB.r = 1.0;
			RGB.g = T / (1.0 + T);
		} else if (H <= 1.001) {
			RGB.r = 1.0;
			RGB.g = 1.0;
		} else if (H <= 2.0) {
			T = tan(A);
			RGB.r = (1.0 + T) / T;
			RGB.g = 1.0;
		} else if (H <= 3.0) {
			T = tan(A);
			RGB.g = 1.0;
			RGB.b = 1.0 + T;
		} else if (H <= 3.999) {
			T = tan(A);
			RGB.g = 1.0 / (1.0 + T);
			RGB.b = 1.0;
		} else if (H <= 4.001) {
			RGB.g = 0.0;
			RGB.b = 1.0;
		} else if (H <= 5.0) {
			T = tan(A);
			RGB.r = -1.0 / T;
			RGB.b = 1.0;
		} else {
			T = tan(A);
			RGB.r = 1.0;
			RGB.b = -T;
		}
		RGB = RGB * V + U;
	}
	return RGB;
}

void fragment() {
	vec2 cUV = UV.xy - vec2(0.5,0.5);
	vec2 cUV2 = cUV * 2.0;
	float d = min( 2, cUV2.x * cUV2.x + cUV2.y * cUV2.y );
	float a = ( PI + atan( cUV2.y, cUV2.x ) ) / ( 2.0 * PI );
//	COLOR = vec4( a, d * 0.5, d * 0.5, 1.0 );
	COLOR = vec4( HCLtoRGB( vec3(a, d, 1.0) ), 1.0 );
}