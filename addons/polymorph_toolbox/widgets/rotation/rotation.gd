extends Node2D

var picker = null
var picker_size = Vector2(200,200)
var picker_active = false
var picker_icon_current = null
var picker_icon_target = null

var rotation_xyz = Vector3( 0,0,0 )
var rotation_target = rotation_xyz
var rotation_constrains = [ Vector2(0,1), Vector2(0,1), Vector2(0,1) ]
var rotation_locks = [ false, false, false ]

var color0 = Vector3( 0,0,0 )
var color1 = Vector3( 0,0,0 )
var constrain0 = Vector2(0,1)
var constrain1 = Vector2(0,1)

var u_color = color0
var v_color = color1
var u_constrain = constrain0
var v_constrain = constrain1

var color_transition = 0.15
var color_transition_current = 0

var mouse_cross = []
var rel_mouse = Vector2()
var last_valid_mouse = Vector2()
var mouse_cross_transition = 0.15
var mouse_cross_transition_current = 0

var _INIT = false

enum PLANE{ NONE, XY, YZ, ZX }
var current_plane = PLANE.NONE

var listener = null

signal rotation_updated

# SETTERS

func set_radians( x, y, z, force = false ):
	
	# normalisation
	rotation_xyz.x = x
	rotation_xyz.y = y
	rotation_xyz.z = z
	rotation_xyz /= TAU

	# clamping
	clamp_rotation()
	
	# applying constrains
	rotation_xyz.x = apply_constrain( rotation_xyz.x, rotation_constrains[0] )
	rotation_xyz.y = apply_constrain( rotation_xyz.y, rotation_constrains[1] )
	rotation_xyz.z = apply_constrain( rotation_xyz.z, rotation_constrains[2] )
	
	if force:
		rotation_target = rotation_xyz
	
	relocate_picker_current( true )

func set_degrees( x, y, z, force = false ):
	
	set_radians( x / 180 * PI, y / 180 * PI, z / 180 * PI, force )
	
func set_constrain( axis, v2 ):
	
	var ax = int(axis)
	if not v2 is Vector2 or ax < 0 or ax > 2:
		return
	
	rotation_constrains[ax] = v2
	match ax:
		0:
			rotation_xyz.x = apply_constrain( rotation_xyz.x, rotation_constrains[0] )
		1:
			rotation_xyz.y = apply_constrain( rotation_xyz.y, rotation_constrains[1] )
		2:
			rotation_xyz.z = apply_constrain( rotation_xyz.z, rotation_constrains[2] )
	
	rotation_target = rotation_xyz
	
	match current_plane:
		PLANE.XY:
			picker_icon_target.position = Vector2( rotation_target.x, rotation_target.y ) * picker_size
		PLANE.YZ:
			picker_icon_target.position = Vector2( rotation_target.y, rotation_target.z ) * picker_size
		PLANE.ZX:
			picker_icon_target.position = Vector2( rotation_target.z, rotation_target.x ) * picker_size
	
	change_plane( current_plane )	
	broadcast()
	color_transition_current = color_transition

func set_lock( axis, enabled ):
	
	var ax = int(axis)
	if not enabled is bool or ax < 0 or ax > 2:
		return
	
	rotation_locks[ax] = enabled
	
	change_plane( current_plane )
	color_transition_current = color_transition

func set_listener( l ):
	if not l is Spatial:
		return
	listener = l

func resize( v2 ):
	
	if not v2 is Vector2:
		return
		
	picker_size = v2
	
	if _INIT:
		$vpc.rect_size = picker_size
		$vpc/vp/picker.rect_size = picker_size
		relocate_picker_current(true)
		$xy.rect_position.y = picker_size.y + 4
		$yz.rect_position.y = picker_size.y + 4
		$zx.rect_position.y = picker_size.y + 4

# GETTERS

func get_eulers():
	return rotation_xyz * 2 * PI

func get_degrees():
	return rotation_xyz * 360

# PROCESSING

func apply_constrain( f, constrain ):
	
	while f < 0:
		f += 1
	while f > 1:
		f -= 1
	
	if constrain != null and (constrain.x != 0 or constrain.y != 1):
		if constrain.x < constrain.y:
			if f < constrain.x or f > constrain.y:
				if f < constrain.x:
					f += 1
				var d = f - constrain.y
				if d > ( 1 - ( constrain.y - constrain.x ) ) * 0.5:
					return constrain.x
				else:
					return constrain.y
		elif constrain.x > constrain.y:
			if f < constrain.x and f > constrain.y:
				var d = f - constrain.y
				if d > ( constrain.x - constrain.y ) * 0.5:
					return constrain.x
				else:
					return constrain.y
	
	return f

func update_picker( force = false ):
	
	if!_INIT:
		return
	
	if color_transition_current == 0:
		u_color = color0
		v_color = color1
		u_constrain = constrain0
		v_constrain = constrain1
		picker.material.set_shader_param( "u_color", u_color )
		picker.material.set_shader_param( "v_color", v_color )
		picker.material.set_shader_param( "u_constrain", u_constrain )
		picker.material.set_shader_param( "v_constrain", v_constrain )
	elif color_transition_current > 0:
		var pc = color_transition_current / color_transition 
		picker.material.set_shader_param( "u_color", u_color * pc + color0 * (1-pc) )
		picker.material.set_shader_param( "v_color", v_color * pc + color1 * (1-pc) )
		var full = Vector2(0,1)
		if pc > 0.5:
			var hpc = (pc-0.5) * 2
			picker.material.set_shader_param( "u_constrain", u_constrain * hpc + full * (1-hpc) )
			picker.material.set_shader_param( "v_constrain", v_constrain * hpc + full * (1-hpc) )
		else:
			var hpc = pc * 2
			picker.material.set_shader_param( "u_constrain", full * hpc + constrain0 * (1-hpc) )
			picker.material.set_shader_param( "v_constrain", full * hpc + constrain1 * (1-hpc) )

func update_target( x, y, z ):
	if x != -1:
		if !rotation_locks[0]:
			rotation_target.x = apply_constrain( x,  rotation_constrains[0] )
	if y != -1:
		if !rotation_locks[1]:
			rotation_target.y = apply_constrain( y,  rotation_constrains[1] )
	if z != -1:
		if !rotation_locks[2]:
			rotation_target.z = apply_constrain( z,  rotation_constrains[2] )

func move_cross( cross, pos, axis ):
	cross.position = pos
	if axis == 'X':
		if cross.position.y - cross.scale.y * 0.5 < 0:
			cross.position.y = cross.scale.y * 0.5
		if cross.position.y + cross.scale.y * 0.5 > picker_size.y:
			cross.position.y = picker_size.y - cross.scale.y * 0.5
	elif axis == 'Y':
		if cross.position.x - cross.scale.x * 0.5 < 0:
			cross.position.x = cross.scale.x * 0.5
		if cross.position.x + cross.scale.x * 0.5 > picker_size.x:
			cross.position.x = picker_size.x - cross.scale.x * 0.5

func update_picker_cross( delta ):
	
	if!_INIT:
		return
	
	if inside_picker() or picker_active:
		
		while rel_mouse.x < 0:
			rel_mouse.x += picker_size.x
		while rel_mouse.x > picker_size.x:
			rel_mouse.x -= picker_size.x
		while rel_mouse.y < 0:
			rel_mouse.y += picker_size.y
		while rel_mouse.y > picker_size.y:
			rel_mouse.y -= picker_size.y
		
		if mouse_cross_transition_current < mouse_cross_transition:
			mouse_cross_transition_current += delta
			if mouse_cross_transition_current > mouse_cross_transition:
				mouse_cross_transition_current = mouse_cross_transition
			var pc = mouse_cross_transition_current / mouse_cross_transition
			mouse_cross[0].scale.y = pc * picker_size.y
			mouse_cross[1].scale.x = pc * picker_size.x
		
		move_cross( mouse_cross[0], rel_mouse, 'X' )
		move_cross( mouse_cross[1], rel_mouse, 'Y' )
		
		last_valid_mouse = rel_mouse
		
		if picker_active:
			match current_plane:
				PLANE.XY:
					update_target( rel_mouse.x / picker_size.x, rel_mouse.y / picker_size.y, -1 )
					picker_icon_target.position = Vector2( rotation_target.x, rotation_target.y ) * picker_size
				PLANE.YZ:
					update_target( -1, rel_mouse.x / picker_size.x, rel_mouse.y / picker_size.y )
					picker_icon_target.position = Vector2( rotation_target.y, rotation_target.z ) * picker_size
				PLANE.ZX:
					update_target( rel_mouse.y / picker_size.y, -1, rel_mouse.x / picker_size.x )
					picker_icon_target.position = Vector2( rotation_target.z, rotation_target.x ) * picker_size
	
	elif mouse_cross_transition_current > 0:
		
		mouse_cross_transition_current -= delta
		if mouse_cross_transition_current < 0:
			mouse_cross_transition_current = 0
		var pc = mouse_cross_transition_current / mouse_cross_transition
		mouse_cross[0].scale.y = pc * picker_size.y
		mouse_cross[1].scale.x = pc * picker_size.x
		
		move_cross( mouse_cross[0], last_valid_mouse, 'X' )
		move_cross( mouse_cross[1], last_valid_mouse, 'Y' )

func relocate_picker_current( apply_on_target ):
	
	if !_INIT:
		return
	
	match current_plane:
		PLANE.XY:
			picker_icon_current.position = Vector2( rotation_xyz.x, rotation_xyz.y ) * picker_size
		PLANE.YZ:
			picker_icon_current.position = Vector2( rotation_xyz.y, rotation_xyz.z ) * picker_size
		PLANE.ZX:
			picker_icon_current.position = Vector2( rotation_xyz.z, rotation_xyz.x ) * picker_size
	
	if apply_on_target:
		match current_plane:
			PLANE.XY:
				picker_icon_target.position = Vector2( rotation_target.x, rotation_target.y ) * picker_size
			PLANE.YZ:
				picker_icon_target.position = Vector2( rotation_target.y, rotation_target.z ) * picker_size
			PLANE.ZX:
				picker_icon_target.position = Vector2( rotation_target.z, rotation_target.x ) * picker_size

func change_plane( p ):
	
	if !_INIT:
		return
	
	if p != current_plane:
		current_plane = p
		relocate_picker_current( true )
	
	match p:
		PLANE.XY:
			# U == X
			if !rotation_locks[0]:
				color0 = Vector3( 1,0,0 )
				constrain0 = rotation_constrains[0]
			else:
				color0 = Vector3()
				constrain0 = Vector2(0,1)
			# V == Y
			if !rotation_locks[1]:
				color1 = Vector3( 0,1,0 )
				constrain1 = rotation_constrains[1]
			else:
				color1 = Vector3()
				constrain1 = Vector2(0,1)
			
		PLANE.YZ:
			# U == Y
			if !rotation_locks[1]:
				color0 = Vector3( 0,1,0 )
				constrain0 = rotation_constrains[1]
			else:
				color0 = Vector3()
				constrain0 = Vector2(0,1)
			# V == Z
			if !rotation_locks[2]:
				color1 = Vector3( 0,0,1 )
				constrain1 = rotation_constrains[2]
			else:
				color1 = Vector3()
				constrain1 = Vector2(0,1)
		
		PLANE.ZX:
			# U == X
			if !rotation_locks[2]:
				color0 = Vector3( 0,0,1 )
				constrain0 = rotation_constrains[2]
			else:
				color0 = Vector3()
				constrain0 = Vector2(0,1)
			# V == Z
			if !rotation_locks[0]:
				color1 = Vector3( 1,0,0 )
				constrain1 = rotation_constrains[0]
			else:
				color1 = Vector3()
				constrain1 = Vector2(0,1)

func _ready():
	
	picker = $vpc/vp/picker
	picker_icon_current = $vpc/vp/current
	picker_icon_target = $vpc/vp/target
	if $vpc/vp/mousex != null:
		mouse_cross.append( $vpc/vp/mousex )
	if $vpc/vp/mousey != null:
		mouse_cross.append( $vpc/vp/mousey )
	_INIT = picker != null and picker_icon_current != null and picker_icon_target != null and mouse_cross.size() == 2
	
	if !_INIT:
		print( "Impossible to run this script smoothly! Check children nodes!" )
		return
	
	change_plane( PLANE.XY )
	color_transition_current = color_transition
	
	mouse_cross[0].scale.y = 0
	mouse_cross[1].scale.x = 0
	
	set_process_input(true)

func inside_picker():
	rel_mouse = get_viewport().get_mouse_position()
	rel_mouse -= get_global_transform().get_origin()
	if rel_mouse.x >= 0 and rel_mouse.x <= picker_size.x and rel_mouse.y >= 0 and rel_mouse.y <= picker_size.y:
		return true
	return false

func shortest_path( current, target, constrain ):
	var right = 0.0
	var left = 0.0
	if current == target:
		return 0
	if current < target:
		right = target - current
		left = 1 - right
	else:
		left = current - target
		right = 1 - left
	# is the interpolation cross the inaccessible area?
	if constrain != null and (constrain.x != 0 or constrain.y != 1):
		if constrain.x < constrain.y:
			if current < target:
				return right
			else:
				return -left
		elif constrain.x > constrain.y:
			if current < target and current < constrain.y and target > constrain.y:
				return -left
			elif current > target and current > constrain.x and target < constrain.x:
				return right
	if left > right:
		return right
	return -left

func rotation_interpolation( delta ):
	
	if rotation_xyz != rotation_target:
		var xm = shortest_path( rotation_xyz.x, rotation_target.x, rotation_constrains[0] )
		var ym = shortest_path( rotation_xyz.y, rotation_target.y, rotation_constrains[1] )
		var zm = shortest_path( rotation_xyz.z, rotation_target.z, rotation_constrains[2] )
		var mm = min( 1, delta * 5 )
		rotation_xyz.x += xm * mm
		rotation_xyz.y += ym * mm
		rotation_xyz.z += zm * mm
		clamp_rotation()
		broadcast()
	
	relocate_picker_current( false )

func _input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed and inside_picker():
			picker_active = true
		else:
			picker_active = false
	elif event is InputEventMouseMotion:
		inside_picker()

func _process(delta):
	
	if!_INIT:
		return
		
	# updating picker
	if color_transition_current > 0:
		color_transition_current -= delta
		if color_transition_current < 0:
			color_transition_current = 0
		update_picker()
	# updating mouse cross
	update_picker_cross( delta )
	# updating rotation
	rotation_interpolation(delta)

func clamp_rotation():
	while rotation_xyz.x < 0:
		rotation_xyz.x += 1
	while rotation_xyz.x > 1:
		rotation_xyz.x -= 1
	while rotation_xyz.y < 0:
		rotation_xyz.y += 1
	while rotation_xyz.y > 1:
		rotation_xyz.y -= 1
	while rotation_xyz.z < 0:
		rotation_xyz.z += 1
	while rotation_xyz.z > 1:
		rotation_xyz.z -= 1

# SIGNALS

func broadcast():
	emit_signal( "rotation_updated" )
	if listener != null:
		listener.rotation = get_eulers()

# SUB PARTS

func _on_xy_pressed():
	change_plane( PLANE.XY )
	color_transition_current = color_transition

func _on_yz_pressed():
	change_plane( PLANE.YZ )
	color_transition_current = color_transition

func _on_xz_pressed():
	change_plane( PLANE.ZX )
	color_transition_current = color_transition