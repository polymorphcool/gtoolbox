shader_type canvas_item;
render_mode blend_mix;

uniform vec3 zero;
uniform vec3 u_color;
uniform vec3 v_color;
uniform vec2 u_constrain;
uniform vec2 v_constrain;

varying vec2 CUV;

void vertex() {
	CUV = VERTEX;
}

void fragment() {
	vec3 uc = mix( zero, u_color, UV.x );
	vec3 vc = mix( zero, v_color, UV.y );
	COLOR = vec4( uc + vc, 1.0 );
	if ( u_constrain.x < u_constrain.y && ( UV.x < u_constrain.x || UV.x > u_constrain.y ) ) {
		COLOR *= vec4( 0.3,0.3,0.3,1.0 );
	} else if ( u_constrain.x > u_constrain.y && UV.x > u_constrain.y && UV.x < u_constrain.x ) {
		COLOR *= vec4( 0.3,0.3,0.3,1.0 );
	} else if ( v_constrain.x < v_constrain.y && ( UV.y < v_constrain.x || UV.y > v_constrain.y ) ) {
		COLOR *= vec4( 0.3,0.3,0.3,1.0 );
	} else if ( v_constrain.x > v_constrain.y && UV.y > v_constrain.y && UV.y < v_constrain.x ) {
		COLOR *= vec4( 0.3,0.3,0.3,1.0 );
	}
}