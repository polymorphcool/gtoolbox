tool

extends Polygon2D

export(int,0,100) var definition = 3 setget _definition
export(float,0,100) var radius = 10 setget _radius
export(float,0,10) var thickness = 1 setget _thickness

func _definition(i):
	definition = i
	regenerate()

func _radius(i):
	radius = i
	regenerate()

func _thickness(i):
	thickness = i
	regenerate()

func regenerate():
	var points = PoolVector2Array()
	for i in range( 0, (definition+1)*2 ):
		if i <= definition:
			var a = i * 2 * PI / definition
			var r = radius + thickness * 0.5
			points.append( Vector2( cos(a) * r, sin(a) * r ) )
		else:
			var a = 2 * PI - ( ( i - ( definition + 1 ) ) * 2 * PI / definition )
			var r = radius - thickness * 0.5
			points.append( Vector2( cos(a) * r, sin(a) * r ) )
			
	polygon = points

func _ready():
	pass
