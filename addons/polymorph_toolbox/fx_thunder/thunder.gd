tool

extends ImmediateGeometry

const GrUP = Vector3(0,1,0) 

const TK_DEFAULT = 0
const TK_ORIGIN = 1
const TK_GROUND = 2

const CONTACT_RESOLUTION = 32

export (bool) var display_plane = true

export (bool) var constant = true
export (float,0,3) var refresh_delay = 1
export (float,0,3) var refresh_after = 1

export (String) var camera_path = null
export (int,1,30) var ray_num = 3
export (int,1,100) var subdivisions = 10
export (float,0.01,10) var segment_length = 0.1
export (bool) var use_segment_length = false
export (float,0,1) var segment_variation = 0.2
export (float,0,2) var main_vibration = 0.3
export (float,0,0.5) var split_chance = 0.3
export (float,0,2) var split_chance_increase = 1.1
export (float,0,1) var split_deviation = 0.1
export (float,0,1) var split_deviation_sustain = 0.9
export (float,0,0.5) var anim_vibration = 0.01

export (float,0,0.5) var plane_width = 0.01

export (float,-1,10) var ground_spread = 1

export (bool) var draw_contact = true

var delay = 0
var restart = 0
var rays = []

var start = null
var ground = null
var ground_basis = null
var gup = null
var dir = null
var dirlength = 1
var plane_z = null
var plane_x = null
var cam = null

var contact_pts = null

var contacts = []

func generate_contact_pts():
	contact_pts = []
	var aoff = PI * 0.25
	var a = TAU / CONTACT_RESOLUTION
	for i in range(0,CONTACT_RESOLUTION):
		var c = cos(  aoff + i * a )
		var s = sin(  aoff + i * a )
		contact_pts.append( Vector3( c, 0.01, s ) )

func thunder_point( pos, k ):
	return {
		'root': [ -1, -1 ], # ray index, point index
		'children': [], # ray indices
		'init': pos,
		'pos': pos,
		'pull': Vector3(),
		'kind': k # TK_DEFAULT, TK_ORIGIN, TK_GROUND
	}

func _ready():
	generate_contact_pts()

func above_ground( pos, gpos, gup ):
	var dist = pos - gpos
	if gup.dot( dist ) > 0:
		return true
	return false

func keep_on_ground( ori, pos, gpos, gup ):
	if not above_ground( pos, gpos, gup ):
		var pos_proj = abs(gup.dot( pos - gpos ))
		var ori_proj = abs(gup.dot( ori - gpos ))
		var r = ori_proj / (ori_proj+pos_proj)
		pos = ori + (pos - ori) * r
	return pos

func generate_ray( startpos, splic, deviation, origin = null ):
	
	var pos = startpos
	var r = [ thunder_point(pos,TK_ORIGIN) ]
	
	if origin != null:
		r[0].root = origin
	
	var splitted = false
	
	while( above_ground( pos, ground.origin, gup ) ):
		
		var tgt = pos + dir + dir * rand_range(-segment_variation,segment_variation) + deviation
		deviation *= split_deviation_sustain
		tgt += plane_x * dirlength * rand_range(-main_vibration,main_vibration)
		tgt += plane_z * dirlength * rand_range(-main_vibration,main_vibration)
		tgt = keep_on_ground( pos, tgt, ground.origin, gup )
		r.append( thunder_point(tgt,TK_DEFAULT) )
		pos = tgt
		
		if randf() < splic:
			splitted = true
			break
		else:
			splic *= split_chance_increase
	
	if len( r ) > 1:
		
		r[ len( r ) - 1 ].kind = TK_GROUND
		
		rays.append( r )
		
		if origin != null:
			rays[origin[0]][origin[1]].children.append( len( rays ) - 1 )
			rays[origin[0]][origin[1]].kind = TK_DEFAULT
		
		if splitted:
			
			var dev = plane_x * dirlength * rand_range(-split_deviation,split_deviation)
			dev += plane_z * dirlength * rand_range(-split_deviation,split_deviation)
			var o = [ len( rays ) - 1, len(r) - 1 ]
			for i in range( 0, 2 ):
				generate_ray( pos, splic / 2, dev, o )
				dev *= -1

func display_contact( p ):
	
	var c = []
	for cp in contact_pts:
		c.append( p + ground.basis.xform( cp ) * dirlength )

	for i in range( 0, CONTACT_RESOLUTION ):
		var j = ( i + 1 ) % CONTACT_RESOLUTION
		add_vertex( c[i] )
		add_vertex( c[j] )

func generate_plane( p0, p1 ):
	
	if cam == null:
		return
	
	var cz = cam.global_transform.basis.z
	
	var pnorm = (p1 - p0).normalized()
	var side = cz.cross( p1- p0 ).normalized() * plane_width
	
	var v0 = p0 - side - pnorm * plane_width
	var v1 = p0 + side - pnorm * plane_width
	var v2 = p1 + side + pnorm * plane_width
	var v3 = p1 - side + pnorm * plane_width
	
	add_vertex( v0 )
	add_vertex( v1 )
	add_vertex( v2 )
	
	add_vertex( v2 )
	add_vertex( v3 )
	add_vertex( v0 )

func backpull_ray( ray, target, length, strength ):
	
	var rl = len(ray)
	
	for i in range( 0, rl ):
		if strength < 1e-5:
			strength = null
			break
		var ii = (rl-1) - i
		var p = ray[ii]
		var pc = min( 1, target.dot( p.init - start.origin ) / length )
		var pull_pt = start.origin + target * length * pc
		var pull_v = pull_pt - p.init
		p.pull += pull_v * strength
		strength *= 0.7
		
	if strength != null and ray[0].root[0] != -1:
		backpull_ray( rays[ ray[0].root[0] ], target, length, strength )

func constrain_contacts():
	
	if ground_spread < 0:
		return
	
	var pull_v3 = ground.origin - start.origin
	var pull_len = pull_v3.length()
	pull_v3 = pull_v3.normalized()
	
	for r in rays:
		var lp = r[ len(r) - 1 ]
		if lp.kind == TK_GROUND:
			var diff = ( ground.origin - lp.pos )
			var d = diff.length()
			if d > ground_spread:
				backpull_ray( r, pull_v3, pull_len, 1 )
	
	for r in rays:
		for tp in r:
			tp.init += tp.pull
			tp.pos += tp.pull
			tp.pull = Vector3()

func _process(delta):
	
	if not visible:
		return
	
	if contact_pts == null:
		generate_contact_pts()
	
	if cam == null and camera_path != null:
		cam = get_node( camera_path )
	
	if constant:
	
		if delay == 0:
		
			start = Transform( $start.transform )
			ground = Transform( $ground.transform )
			gup = ground.basis.xform( GrUP )
			dir = (ground.origin - start.origin) / subdivisions
			if use_segment_length and segment_length > 0:
				dir = dir.normalized() * segment_length
			dirlength = dir.length()
			plane_z = Vector3(1,0,0).cross( dir ).normalized()
			plane_x = dir.cross( plane_z ).normalized()
			
			rays.clear()
			var ri = 0
			while( ri < ray_num ):
				generate_ray( start.origin, split_chance, Vector3( 0,0,0 ) )
				ri += 1
			
#			constrain_contacts()
			
			delay = refresh_delay
			
			contacts = []
			for r in rays:
				var lp = r[ len(r) - 1 ]
				if lp.kind == TK_GROUND:
					contacts.append( lp.pos )
		
		else:
			
			for r in rays:
				for tp in r:
					if tp.kind == TK_DEFAULT:
						tp.pos = tp.init + Vector3( 
							rand_range(-anim_vibration,anim_vibration), 
							rand_range(-anim_vibration,anim_vibration), 
							rand_range(-anim_vibration,anim_vibration) 
							) * dirlength
					elif tp.kind == TK_ORIGIN and tp.root[0] != -1:
						tp.pos = rays[tp.root[0]][tp.root[1]].pos
						pass
	
	clear()
	
	if not display_plane:
		begin( Mesh.PRIMITIVE_LINES )
		for r in rays:
			for i in range( 1, len(r) ):
				add_vertex( r[i-1].pos )
				add_vertex( r[i].pos )
		end()
	else:
		begin( Mesh.PRIMITIVE_TRIANGLES )
		for r in rays:
			for i in range( 1, len(r) ):
				generate_plane( r[i-1].pos, r[i].pos )
		end()
	
	begin( Mesh.PRIMITIVE_LINES )
	for r in rays:
		var lp = r[ len(r) - 1 ]
		if lp.kind == TK_GROUND and draw_contact:
			display_contact( lp.pos )
	end()
	
	if constant:
		
		delay -= delta
		
		if delay < 0:
			if restart < 0:
				delay = 0
				restart = refresh_after
			else:
				if len( rays ) > 0:
					rays = []
				restart -= delta
