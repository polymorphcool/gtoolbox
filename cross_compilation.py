#!/usr/bin/python3

import os

'''
generates all build (cross-compilation)
adapt script to land in folder containing  the repo: https://github.com/godotengine/godot
'''

# ---- linux

# ---- raspbian
'''
$ wget https://gist.githubusercontent.com/silverkorn/f0566819025d4a4882b82b2b27d20826/raw/godot-cross-compile.sh
$ sudo chmod u+x godot-cross-compile.sh
$ sudo ./godot-cross-compile.sh rpi3 raspbian platform=x11 -j 4
'''

# ---- windows 
# install mingw
'''
$ sudo apt install mingw-w64
'''
# -> verify POSIX!
'''
$ sudo update-alternatives --config x86_64-w64-mingw32-gcc
>>> choose x86_64-w64-mingw32-gcc-posix
$ sudo update-alternatives --config x86_64-w64-mingw32-g++
>>>> x86_64-w64-mingw32-g++-posix
'''

# ---- osx

'''
git clone --depth=1 https://github.com/tpoechtrager/osxcross.git "$HOME/osxcross"
'''

# step 1:
# see https://github.com/tpoechtrager/osxcross#packaging-the-sdk
# warning: XCode is ~10G!

'''
sudo apt install clang make liblzma-dev libssl-dev lzma-dev libxml2-dev llvm-dev uuid-dev

./tools/gen_sdk_package_pbzx.sh <xcode>.xip

copy or move the SDK into the tarballs/ directory
'''

# check cmake version
'''
cmake --version
'''
# if it´s < 3.2.3, do (?)
'''
curl -sSL https://cmake.org/files/v3.14/cmake-3.14.5-Linux-x86_64.tar.gz | sudo tar -xzC /opt
'''

# valid if current script is run from forge.godot/polymorph/gtoolbox
os.chdir( os.path.join( os.getcwd(), "../../godot_3x" ) )

# ---- windows 
## editor
os.system( "scons platform=windows -u --jobs=16 tools=yes debug_symbols=yes module_ffmpeg_enabled=no target=release_debug bits=64" )
## 64 release
#os.system( "scons platform=windows -u --jobs=16 tools=no debug_symbols=no tools=no module_ffmpeg_enabled=no target=release bits=64" )
## 64 release_debug
#os.system( "scons platform=windows -u --jobs=16 tools=no debug_symbols=no tools=no module_ffmpeg_enabled=no target=release_debug bits=64" )

# ---- linux
## 64 release
#os.system( "scons platform=linuxbsd -u --jobs=16 tools=no debug_symbols=no target=release bits=64" )
## 64 release_debug
#os.system("scons platform=linuxbsd -u --jobs=16 tools=no target=release_debug bits=64")
## 32 release
#os.system("scons platform=x11 -u --jobs=16 tools=no target=release bits=32")
## 32 release_debug
#os.system("scons platform=x11 -u --jobs=16 tools=no target=release_debug bits=32")
