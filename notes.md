# IDE: eclipse + cute scons
https://www.eclipse.org/cdt/
https://cute-test.com/guides/scons/


#!/bin/bash

# install dependencies for godot
sudo apt-get install build-essential scons pkg-config libx11-dev libxcursor-dev libxinerama-dev libgl1-mesa-dev libglu-dev libasound2-dev libpulse-dev libudev-dev libxi-dev libxrandr-dev yasm clang-7

# clone repo (requires a ssh key)
cd ..
git clone git@github.com:godotengine/godot.git

# compile godot (might take a while...)
cd godot
scons -j8 platform=linuxbsd

# launch godot



