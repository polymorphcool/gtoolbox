import os
from shutil import copyfile

# folder containing the source code of the engine
target = "../godot"

current = os.path.dirname(os.path.realpath(__file__))
folders = ['main'] #,'scene']

print( current )

for fo in folders:
	
	for root, subfolder, files in os.walk(os.path.join(current,fo)):
		for f in files:
			
			subd = root[ len(current)+1: ]
			
			target_f = os.path.join( target, subd, f )
			current_f = os.path.join( root, f )
			
			copyfile(current_f, target_f)
			print( "copy", current_f, "to", target_f )

# replacing fonts
font_path = 'thirdparty/fonts/Hack_Regular.ttf'
target_0 = 'thirdparty/fonts/NotoSansUI_Bold.ttf'
target_1 = 'thirdparty/fonts/NotoSansUI_Regular.ttf'
copyfile( os.path.join( target, *font_path.split('/')), os.path.join( target, *target_0.split('/')))
copyfile( os.path.join( target, *font_path.split('/')), os.path.join( target, *target_1.split('/')))